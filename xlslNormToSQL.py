from io import TextIOWrapper
from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet

def write_script_to_file(table_name: str, collumnLetter: str, sql_file: TextIOWrapper, sheet: Worksheet):
  insert_script = f'INSERT INTO public.{table_name} (identifier, "name", category, description) values \n'  
  cell_values = []
  values = []
  
  skip = 0
  
  for celula in sheet[collumnLetter]:
    if (skip < 3):
      skip += 1
    else:
        cell_values.append(celula.value)
        
  for i in range(0, len(cell_values), 4):
    if (cell_values[i] == None):
      break
    if (i != 0):
      values.append(",\n")
    try:
      value1 = str(cell_values[i]).replace("'", '"')
      value2 = str(cell_values[i + 1]).replace("'", '"')
      value3 = str(cell_values[i + 2]).replace("'", '"')
      value4 = str(cell_values[i + 3]).replace("'", '"')
      values.append(f"('{value1}', '{value2}', '{value3}', '{value4}')")
    except:
      print()
    finally:
      print("-")
      
    
    
    
  if len(values) > 1:
    values.append(";\n")
    values.append("\n")
    sql_file.write("\n")
    sql_file.write(insert_script)
    sql_file.writelines(values)
    
def write_to_file(filename: str):
  wb = load_workbook(filename = f'{filename}.xlsx') 

  sheet = wb.active

  with open(f"{filename}.sql", 'w') as sql_file:
    norm_name = sheet["A1"].value
    norm_description = sheet["B1"].value
    
    create_norm = f"insert into dp_norm (\"name\", description) values ('{norm_name}', '{norm_description}'); \n"
    
    sql_file.write(create_norm)
    
    write_script_to_file("dp_threat", "A", sql_file, sheet)
    write_script_to_file("dp_vulnerability", "B", sql_file, sheet)
    write_script_to_file("dp_control", "C", sql_file, sheet)
  
if (__name__ == "__main__"):
  filename = [
    "iso27701-controller",
    "iso27001",
    "iso27701-operador",
    "iso29100",
    "lgpd",
    "nist",
    "norma-27005",
    "soc2",
    ]
  
  for st in filename:
    write_to_file(st)
  
    